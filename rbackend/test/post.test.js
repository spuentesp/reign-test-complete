import httpStatus from 'http-status';
import { expect } from 'chai';
import { some, omitBy, isNil } from 'lodash';
import axios from "axios";
import sinon from 'sinon';

import News from '../src/api/models/news';
import ErrorHandler from '../src/api/utils/errorHandler';
import NewsController from '../src/api/controllers/news';
import HackerNewsService from '../src/api/services/hackerNewsService';


describe('Common utils', async () => {
    let error;

    beforeEach(async () => {
        let error = {error:'error'}
       return true;
    });

    describe('Error Handler', () => {

        it('should log an error and return true', () => {
            expect(ErrorHandler.handleError(error)).to.equal(true)
            return true;
        });
    })
})

describe('Controllers', async () => {
   
    describe('NewsController', () => {
        let nc;
        let emptyArray 
        beforeEach(async () => {
            nc = new NewsController();
            emptyArray = [];
            sinon.stub(News, 'find').resolves(emptyArray);
            sinon.stub(News, 'update').resolves(true);
            sinon.stub(News, 'insertMany').resolves(true);
        });
    
        afterEach(function() {
            News.find.restore();
            News.update.restore();
            News.insertMany.restore();
        });

        it('should call the method from the News model and return an empty list', async () => {
            let res = await nc.get();
            sinon.assert.calledOnce(News.find);
            return expect(res).to.equal(emptyArray)     
        });

        it('should call insertmany and return true',async () => {
            let res = await nc.create()
            sinon.assert.calledOnce(News.insertMany);
            return expect(res).to.equal(true)     
        });
        it('should call update in model and return true',async () => {
            let oID = 0;
            let res = await nc.remove(oID);
            sinon.assert.calledOnce(News.update);
            return expect(res).to.equal(true)     
        })
    })
})

describe('Services', async () => {
   
    describe('HackerNewsService', () => {
        let hns;
        let response; 
        beforeEach(async () => {
            hns = new HackerNewsService();
            response = {
                data: ''
            };
            sinon.stub(axios, 'get').resolves(response);
        });
    
        afterEach(function() {
            axios.get.restore();
        });

        it('should load an empty list from axios', async () => {
            let res = await hns.getHackerNews();
            sinon.assert.calledOnce(axios.get);
            return expect(res).to.equal(response.data)     
        });

    })
})