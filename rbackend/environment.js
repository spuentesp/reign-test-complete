module.exports = {
  development: {
    port: 3000, // assign your own port no
    mongoUri: 'mongodb://localhost/reigntst',
    logs: 'dev'
  },
  production: {
    port: 3000, // assign your own port no
    mongoUri: 'mongodb://localhost/reigntst',
    logs: 'combined'
  },
  test: {
    port: 3000, // assign your own port no
    mongoUri: 'mongodb://localhost/reigntst',
    logs: 'dev'
  }
};

