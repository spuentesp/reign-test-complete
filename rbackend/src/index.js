import 'babel-polyfill';
import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';

import environment from '../environment';
import mongoose from './config/mongoose';
import routes from './api/routes/';
import HackerNewsJob from './api/jobs/hackerNewsJob'
import Jobs from './api/jobs/jobs';
// getting application environment
const env = process.env.NODE_ENV;

// getting application config based on environment
const envConfig = environment[env];

// setting port value
const PORT = envConfig.port || 3000;

/**
* Express instance
* @public
*/
const app = express();

// open mongoose connection
mongoose.connect(envConfig, env);

// request logging. dev: console | production: file
app.use(morgan(envConfig.logs));

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// mount api routes
app.use('/', routes);

// listen to requests
app.listen(PORT);

const job = new Jobs();
job.schedule();

module.exports = app;