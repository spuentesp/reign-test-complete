const config = {
    api: {
        HACKERNEWS_NODEJS: 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs'
    },
    cron:{
        HACKERNEWS_TIMER: '* 30 * * * *'
    }
}

export default config;