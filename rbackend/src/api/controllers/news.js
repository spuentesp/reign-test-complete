import httpStatus from 'http-status';
import { omit } from 'lodash';
import News from '../models/news';
import ErrorHandler from '../utils/errorHandler'
export default class NewsController {
    
    /**
     * Get News
     * @public
     */
    get = async () => {
        let news = await News.find({ "deleted": { "$ne": true }, "story_title":{ "$ne": '' },"story":{ "$ne": '' }});
        return news;
    };
    
    
    /**
     * Create new News
     * @public
     */
    create = async (news) => {
        try {
            let insert = await News.insertMany(news,{
                ordered: false
             });
            return true;
        } catch (error) {
            if (error.name === 'BulkWriteError' && error.code === 11000) {
                console.log('noticias duplicadas. ignorando');
              } else {
                throw new Error(error);
              }
        
            
        }
    };
    
    /**
     * Delete News
     * @public
     */
    remove = async (objID) => {
        console.log(objID)
        try{
            let rem = await News.update({'objectID':{'$eq':objID}},{"deleted":true});
            return rem;
        }catch(e){
            ErrorHandler.handleError(e)
        }
    };
 }
