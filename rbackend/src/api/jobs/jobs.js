import * as cron from 'node-cron'
import config from '../config/config'
import HackerNewsJob from  './hackerNewsJob'
export default class Jobs {

    constructor(){};
    hackerNewsJob = new HackerNewsJob();

    schedule(){
        this.hackerNewsJob.schedule();
    }

}