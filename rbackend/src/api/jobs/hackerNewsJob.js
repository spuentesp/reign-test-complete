import * as cron from 'node-cron'
import config from '../config/config'
import HackerNewsService from '../services/hackerNewsService'
import NewsController from '../controllers/news'
export default class HackerNewsJob {



    async schedule(){
        console.log(cron.validate(config.cron.HACKERNEWS_TIMER));
        cron.schedule(config.cron.HACKERNEWS_TIMER,this.job);
        console.log('job scheduled');
    }

    async job() {
        //definido local para que lo vea el contexto del cron
        let LIST_FIELD = 'hits';
        this.hns = new HackerNewsService();
        this.newsController = new NewsController();
        let hackerNews = await this.hns.getHackerNews();
        this.newsController.create(hackerNews[LIST_FIELD])

    }

}