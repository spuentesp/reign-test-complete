import mongoose from 'mongoose';
import upsertMany from '@meanie/mongoose-upsert-many'
const NewsSchema = new mongoose.Schema({
  created_at: {
    type: Date,
    default: Date.now
  },
  title:{
    type: String,
    default: ''
  },
  url: {
    type: String,
    default: ''
  },
  author: {
    type: String,
    default: ''
  },
  points: {
    type: Number,
    default: 0
  },
  story_text: {
    type: String,
    default: ''
  },
  comment_text: {
    type: String,
    default: ''
  },
  num_comments: {
    type: Number,
    default: 0
  },
  story_id: {
    type: Number,
    default: 0
  },
  story_title: {
    type: String,
    default: ''
  },
  story_url: {
    type: String,
    default: ''
  },parent_id: {
    type: Number,
    default: 0
  },
  created_at_i: {
    type: Number,
    default: 0
  },tags:[{
      type:String
  }],objectID: {
    type: Number,
    default: 0,
    unique : true,
     required : true,
      dropDups: true  
  },
  deleted: {
    type:Boolean,
    required:true,
    default:false
  }
});
export default mongoose.model('News', NewsSchema);