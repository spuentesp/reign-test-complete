import HackerNewsService from "./HackerNewsService";
import NewsController from "../controllers/news";

export default class  JobService{

    
    //fix para funcionamiento de jobs, controladores y servicios adentro.
    async getNewsJob(){
     const newsController = new NewsController();
     const hackerNewsService = new HackerNewsService();
     let hackerNews = await hackerNewsService.getHackerNews();
     let hits = hackerNews.hits;
     await newsController.create(hits);
     console.log('created!');
    }
}