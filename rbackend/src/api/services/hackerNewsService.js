import axios from "axios"
import config from "../config/config"


export default class HackerNewsService{

    async getHackerNews() {
        try{
            let response = await axios.get(config.api.HACKERNEWS_NODEJS);
            let data = response.data;
            return data;
        }catch(e){
            throw new Error(e);
        }
        
    }
}

