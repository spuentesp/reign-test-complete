import { Router } from 'express';

import newsRoute from './news';

const router = Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

router.use('/news', newsRoute);

module.exports = router;