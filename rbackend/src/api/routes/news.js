import { Router } from 'express';
import NewsController from '../controllers/news';

const router = Router();
const newsController = new NewsController();

router.get('/', (req, res) => res.send('News yay!'));

router.get('/all', async (req, res) => {
    let response = await newsController.get()
    res.send(response)
});

router.post('/delete', async (req, res) => {
    let objectId = req.body.objectId;
    if(typeof objectId !== 'undefined' && objectId !== null) {
        let response = await newsController.remove(objectId)
        res.send(response)
    }else{
        res.status(400).send();
    }
    
});

module.exports = router;