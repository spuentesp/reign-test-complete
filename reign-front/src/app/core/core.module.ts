import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpconnectionService } from './services/httpconnection.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers:[HttpconnectionService]
})
export class CoreModule { }
