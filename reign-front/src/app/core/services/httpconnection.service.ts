import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpconnectionService {

  constructor(private http: HttpClient) { }


  get(url:string){
    return this.http.get(url)
  }

  post(url:string,body:object){
    return this.http.post(url,body)
  }
}
