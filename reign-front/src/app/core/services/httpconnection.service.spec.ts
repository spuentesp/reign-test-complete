import { TestBed } from '@angular/core/testing';

import { HttpconnectionService } from './httpconnection.service';

describe('HttpconnectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpconnectionService = TestBed.get(HttpconnectionService);
    expect(service).toBeTruthy();
  });
});
