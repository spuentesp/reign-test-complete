import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './components/about/about.component';
import { AboutRoutingModule } from './about-routing.module';
import { MatCardModule } from '@angular/material';



@NgModule({
  declarations: [AboutComponent],
  imports: [
    AboutRoutingModule,
    CommonModule,
    MatCardModule
  ]
})
export class AboutModule { }
