import { Injectable } from '@angular/core';
import { HttpconnectionService } from 'src/app/core/services/httpconnection.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewslistService {

  constructor(private httpConn: HttpconnectionService) { }

  getNewsList(){
    return this.httpConn.get(environment.api.ENDPOINT+environment.api.routes.news.all);
  }

  deleteNews(oID){
    return this.httpConn.post(environment.api.ENDPOINT+environment.api.routes.news.delete,{objectId:oID});
  }
}
