import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsListComponent } from './components/news-list/news-list.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: NewsListComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule]
})
export class NewsListRoutingModule { }
