import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  @Input('post') post;
  @Output('delete') selected = new EventEmitter<object>();
  constructor() { }

  ngOnInit() {
  }

  delete(oID){
    this.selected.emit(oID);
  }


}
