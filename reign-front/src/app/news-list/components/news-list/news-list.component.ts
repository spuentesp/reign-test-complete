import { Component, OnInit } from '@angular/core';
import { NewslistService } from '../../services/newslist.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {

  ELEMENT_DELETED_MSJ = 'element deleted';
  ERROR_ELEMENT_MSJ = 'error deleting message';
  public result : any[];

  constructor(private nls:NewslistService) { }
  ngOnInit() {
    this.nls.getNewsList().subscribe((res:any[])=>console.log(this.result=res))
  }

  async deleteItem(oID){
    let del = await this.nls.deleteNews(oID).toPromise();
    if(del['ok'] === 1){
      alert(this.ELEMENT_DELETED_MSJ);
      this.result = this.result.filter(function(item) {
        return item.objectID !== oID;
    })
    }else{
      alert(this.ERROR_ELEMENT_MSJ)
    }
  }


}
