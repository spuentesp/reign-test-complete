import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsListComponent } from './components/news-list/news-list.component';
import { NewsComponent } from './components/news/news.component';
import { NewsListRoutingModule } from './newslist-routing.module';
import { CoreModule } from '../core/core.module';
import {MatListModule, MatIconModule} from '@angular/material';

@NgModule({
  declarations: [NewsListComponent, NewsComponent],
  imports: [
    NewsListRoutingModule,
    CommonModule,
    CoreModule,
    MatListModule,
    MatIconModule
  ],
  exports:[NewsListComponent]
})
export class NewsListModule { }
