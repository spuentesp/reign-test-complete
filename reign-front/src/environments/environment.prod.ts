export const environment = {
  production: true,
  api:{
    ENDPOINT:'http://localhost:3000',
    routes:{
      news:{
        all: '/news/all',
        delete:'/news/delete'
      }
    }
  }
};